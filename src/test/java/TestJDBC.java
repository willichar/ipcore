import lesson15.Main;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.PostgreSQLContainer;
import java.sql.*;
import static org.mockito.Mockito.*;


public class TestJDBC {
    
    static PostgreSQLContainer postgres;
    Connection con;

    @BeforeEach
    public void connectionToDB() {

        try {
            postgres = new PostgreSQLContainer<>("antilya/unitdb:latest")
                    .withDatabaseName("postgres")
                    .withUsername("postgres")
                    .withPassword("postgres");
            postgres.start();
            con = DriverManager.getConnection(postgres.getJdbcUrl(), postgres.getDatabaseName(), postgres.getPassword());

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @AfterEach
    public void dropTestTable() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            postgres.stop();
        }


    }

    @Test
    public void testPrepareStatement() {
        try {

            Main.prepareStatement(con);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from \"user\"");
            while (rs.next()) {
                Assertions.assertEquals("Tom3", rs.getString(2).trim());
                Assertions.assertEquals("01.01.01", rs.getString(3).trim());
                Assertions.assertEquals("Tom007", rs.getString(4).trim());
                Assertions.assertEquals("Chicago", rs.getString(5).trim());
                Assertions.assertEquals("tom@gmail.com", rs.getString(6).trim());
                Assertions.assertEquals("descriprion", rs.getString(7).trim());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testBatchProcessing() {

        try {
            Main.batchProcessing(con);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from \"role\"");
            while (rs.next()) {
                switch (rs.getString(2).trim()) {
                    case "Administration":
                        Assertions.assertEquals("Administration", rs.getString(2).trim());
                        break;
                    case "Billing":
                        Assertions.assertEquals("Billing", rs.getString(2).trim());
                        break;
                    case "Clients":
                        Assertions.assertEquals("Clients", rs.getString(2).trim());
                        break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void testBatchProcessingMock() {

        try {
            ResultSet rs = mock(ResultSet.class);
            when(rs.getString(2)).thenReturn("Administration");
            when(rs.next()).thenReturn(true).thenReturn(false);
            while (rs.next()) {
                switch (rs.getString(2).trim()) {
                    case "Administration":
                        Assertions.assertEquals("Administration", rs.getString(2).trim());
                        break;
                    case "Billing":
                        Assertions.assertEquals("Billing", rs.getString(2).trim());
                        break;
                    case "Clients":
                        Assertions.assertEquals("Clients", rs.getString(2).trim());
                        break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
































