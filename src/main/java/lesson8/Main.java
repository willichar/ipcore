package lesson8;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        readFromConsole();
        File file = new File("SomeClass.java");
        javac(file);

        ClassLoader cl = new MyClassLoader();
        Class <?> cll = cl.loadClass("SomeClass");
        Worker w = (Worker) cll.newInstance();
        w.doWork();


    }

    static void javac(File fileJava) {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, fileJava.getPath());

    }

    static void readFromConsole() {

        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(System.in);
        sb.append("import lesson8.Worker; \n public class SomeClass implements Worker {\n" +
                "\n" +
                "    public void doWork() {");
        while (true) {
            String inputStr =  scanner.nextLine();
            if (inputStr.equals("")) {
                break;
            }
            sb.append(inputStr);
        }
        sb.append("}}");
        createJavaFile(sb.toString());
    }

    static void createJavaFile(String str) {
        try (FileOutputStream fos = new FileOutputStream("SomeClass.java")) {
            fos.write(str.getBytes());
        } catch (IOException ignore) {

        }
    }


}


