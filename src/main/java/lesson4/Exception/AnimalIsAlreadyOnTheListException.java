package lesson4.Exception;

public class AnimalIsAlreadyOnTheListException extends Exception{
    public AnimalIsAlreadyOnTheListException(String str){
        super("Животное уже есть в списке:" + str);
    }
}

