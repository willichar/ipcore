package lesson4;

import lesson2.task3.Person;
import lesson4.Exception.AnimalIsAlreadyOnTheListException;

import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class CardFile {
    private Map<Integer, Pet> cardFile = new HashMap<>();
    private Map<String, List<Pet>> findPetMap = new HashMap<>();

    /**
     * Метод добавления животного в общий список (учесть, что добавление дубликатов должно приводить к исключительной ситуации)
     *
     * @param pet - петомец
     * @throws AnimalIsAlreadyOnTheListException
     */

    public void addPet(Pet pet) throws AnimalIsAlreadyOnTheListException {
        if (cardFile.containsValue(pet)) {
            throw new AnimalIsAlreadyOnTheListException(pet.getId() + " " + pet.getName());
        }
        cardFile.put(pet.getId(), pet);
        List<Pet> pets = findPetMap.get(pet.getName());
        if (isNull(pets)) {
            pets = new ArrayList<>();
        }
        pets.add(pet);
        findPetMap.put(pet.getName(), pets);


    }

    /**
     * Поиск животного по его кличке (поиск должен быть эффективным)
     *
     * @param name - кличка
     * @return
     */
    public List<Pet> findPet(String name) {

        List<Pet> findList = findPetMap.get(name);
        return findList;
    }

    /**
     * Изменение данных животного по его идентификатору
     *
     * @param id        - уникальный идентификатор
     * @param newName   - новая кличка , если null оставляем старое значение
     * @param newWeight - новый вес ,  если null оставляем старое значение
     * @param person    - новый хозяин , если null оставляем старое значение
     */
    public void editPet(int id, String newName, Integer newWeight, Person person) {
        Pet pet = cardFile.get(id);
        if (nonNull(newName)) { //
            pet.setName(newName);
        }
        if (nonNull(newWeight)) {
            pet.setWeight(newWeight);
        }

        if (nonNull(person)) {
            pet.setOwner(person);
        }


    }

    public static int utilityCompare(int i, int j, List<Pet> sortListPet, SortMarker sortMarker) {
        switch (sortMarker) {
            case NAME:
                return sortListPet.get(i).getName().compareTo(sortListPet.get(j).getName());
            case OWNWER:
                return sortListPet.get(i).getOwner().getName().compareTo(sortListPet.get(j).getOwner().getName());
            case WEIGHT:
                return sortListPet.get(i).getWeight() > sortListPet.get(j).getWeight() ? 0 : 1;
        }
        return 0;
    }

    /**
     * Сортировка, поля для сортировки –  хозяин, кличка животного, вес
     *
     * @param field
     */
    public void sort(SortMarker field) {
        List<Pet> sortListPet = new ArrayList<>();
        for (Map.Entry<Integer, Pet> p : cardFile.entrySet()) {
            sortListPet.add(p.getValue());
        }
        boolean b = false;
        while (!b) {
            b = true;
            for (int i = 0; i < sortListPet.size(); i++) {
                for (int j = i + 1; j < sortListPet.size(); j++) {
                    if (utilityCompare(i, j, sortListPet, field) > 0) {
                        Pet pb = sortListPet.get(j);
                        sortListPet.set(j, sortListPet.get(i));
                        sortListPet.set(i, pb);
                        b = false;
                    }
                }

            }
        }


        outSort(sortListPet);
    }

    public void outSort(List<Pet> list) {
        StringBuilder strOut = new StringBuilder();
        for (Pet p : list) {
            strOut.append(p);
        }
        System.out.println(strOut.toString());
    }

    @Override
    public String toString() {
        sort(SortMarker.NAME);
        return null;
    }
}
