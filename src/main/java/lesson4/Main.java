package lesson4;

import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;
import lesson2.task3.Person;
import lesson4.Exception.AnimalIsAlreadyOnTheListException;

import java.io.IOException;
import java.util.Random;


public class Main {

    CardFile cardFile = new CardFile();
    Person[] owners = new lesson2.task3.Main().genData(50);

    private Person findNamePerson(String name){
        for (Person p : owners){
            if (p.getName().equals(name)){
                return p;
            }
        }
        return null;
    }


    /**
     * Добавляет животное в мапу
     * Пример: addpet 54 Barsik 8 88
     *
     * @param id
     * @param name
     * @param owner
     * @param weight
     * @throws AnimalIsAlreadyOnTheListException
     */
    @Command(description = "Добавляет животное в мапу")
    public void addpet(int id, String name, String owner, int weight) throws AnimalIsAlreadyOnTheListException {
        Pet pet1 = new Pet(id, name, findNamePerson(owner), weight);
        cardFile.addPet(pet1);
    }


    /**
     * Выводит список животных
     * <p>
     * Пример: listpet
     *
     * @return
     */
    @Command(description = "Выводит список животных")
    public String listpet() {
        return cardFile.toString();
    }


    /**
     * Поиск животного по его кличке
     * <p>
     * Пример: findpet Murzik
     *
     * @throws AnimalIsAlreadyOnTheListException
     */

    @Command(description = "Ищет животное по кличке")
    public String findpet(String name) {
        try {
            return cardFile.findPet(name).toString();
        } catch (NullPointerException ex) {
            System.out.println(String.format("Питомца по кличке %s не найдено", name));
        }
        return null;
    }


    /**
     * Генерирует данные
     * <p>
     * Пример: genpet
     *
     * @throws AnimalIsAlreadyOnTheListException
     */
    @Command(description = "Выводит список животных")
    public void genpet() throws AnimalIsAlreadyOnTheListException {
        Random r = new Random();
        Pet pet1 = new Pet(r.nextInt(), "Busya", owners[r.nextInt(10)], r.nextInt(100));
        Pet pet2 = new Pet(r.nextInt(), "Artemon", owners[r.nextInt(10)], r.nextInt(100));
        Pet pet3 = new Pet(r.nextInt(), "Barsik", owners[r.nextInt(10)], r.nextInt(100));
        Pet pet4 = new Pet(r.nextInt(), "Yana", owners[r.nextInt(10)], r.nextInt(100));
        Pet pet5 = new Pet(r.nextInt(), "Sharik", owners[r.nextInt(10)], r.nextInt(100));
        cardFile.addPet(pet1);
        cardFile.addPet(pet2);
        cardFile.addPet(pet3);
        cardFile.addPet(pet4);
        cardFile.addPet(pet5);

    }

    /**
     * Выводит на экран список животных отсортированных по полям хозяин, кличка животного, вес (name/weight/owner).
     * <p>
     * Пример: sortcard name (сортировка по имени)
     *
     * @param name
     */
    @Command(description = "Сортирует список")
    public void sortcard(String name) {
        if (name.equals("name")) {
            cardFile.sort(SortMarker.NAME);
        }else if (name.equals("owner")) {
            cardFile.sort(SortMarker.OWNWER);
        }else if (name.equals("weight")) {
            cardFile.sort(SortMarker.WEIGHT);
        } else System.out.println(":(");
    }


    /**
     * изменение данных животного по его идентификатору
     * <p>
     * Пример: editpet 1 Murzik 5 4
     *
     * @param id
     * @param name
     * @param weight
     * @param person
     */
    @Command(description = "Редактирует животное")
    public void editpet(int id, String name, Integer weight, int person) {
        cardFile.editPet(id, name, weight, owners[person]);
    }


    public static void main(String[] args) throws AnimalIsAlreadyOnTheListException, IOException {

        ShellFactory.createConsoleShell("hello", "", new lesson4.Main())
                .commandLoop();

    }


}
