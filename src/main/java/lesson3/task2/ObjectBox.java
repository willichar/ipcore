package lesson3.task2;

import java.util.ArrayList;
import java.util.List;

public class ObjectBox <T extends Number> {


    protected List<? super Object> setMas = new ArrayList<Object>();

    public void addObject(T o) {
        setMas.add(o);
    }

    public void deleteObject(T o) {
        setMas.remove(o);
    }

    public void dump() {
        for (Object o : setMas) {
            System.out.print(o + "  ");
        }
    }
}
