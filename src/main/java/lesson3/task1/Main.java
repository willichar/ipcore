package lesson3.task1;

public class Main {
    public static void main(String[] args) throws Exception {

        Number[] mas = {152.66, 3, 4, 5, 6, 8, 80, 25, 150};
        MathBox mathBox = new MathBox(mas);
        mathBox.addObject(55.0f); // проверка метода суперкласса ObjectBox
        mathBox.addObject(55000l);
        mathBox.deleteObject(152.66); // проверка метода суперкласса ObjectBox
        mathBox.removeElement(5);
        System.out.println("Сумма всех элементов коллекции: " + mathBox.summator()); // проверка summator
        System.out.println(mathBox);
        mathBox.splitter(150); // проверка  splitter
        mathBox.dump();// проверка метода суперкласса ObjectBox
    }
}
