package lesson3.task1;

import lesson3.task2.ObjectBox;

import java.util.*;

public class MathBox extends ObjectBox {

    public MathBox(Number[] numbers) throws Exception {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (i != j && numbers[i].equals(numbers[j])) {
                    System.out.println(String.format("Дублируеться число %s", numbers[i]));
                    throw new Exception();
                }
            }
        }

        setMas.addAll(Arrays.asList(numbers));


    }


    /**
     * Возвращающий сумму всех элементов коллекции
     *
     * @return - сумма всех элементов коллекции
     */
    public double summator() throws Exception {
        double i = 0;
        for (Object j : setMas) {
            switch (j.getClass().getSimpleName()) {
                case "Double":
                    i = i + (Double) j;
                    break;
                case "Integer":
                    i = i + (Integer) j;
                    break;
                case "Float":
                    i = i + (Float) j;
                    break;
                case "Long":
                    i = i + (Long) j;
                    break;
                case "Byte":
                    i = i + (Byte) j;
                    break;
                case "Short":
                    i = i + (Short) j;
                    break;
            }
        }
        return i;
    }

    /**
     * Поочередное деление всех хранящихся в объекте элементов на делитель
     *
     * @param del - делитель
     */
    public void splitter(double del) {
        int i = 0;

        for (Object j : setMas) {
            switch (j.getClass().getSimpleName()) {
                case "Double":
                    Double newEl = (Double) j / del;
                    setMas.set(i, newEl);
                    i++;
                    break;
                case "Integer":
                    Double newElInt = (Integer) j / del;
                    setMas.set(i, newElInt);
                    i++;
                    break;
                case "Float":
                    Double newElFlo = (Float) j / del;
                    setMas.set(i, newElFlo);
                    i++;
                    break;
                case "Long":
                    Double newElLon = (Long) j / del;
                    setMas.set(i, newElLon);
                    i++;
                    break;
                case "Byte":
                    Double newElByt = (Long) j / del;
                    setMas.set(i, newElByt);
                    i++;
                    break;
                case "Short":
                    Double newElShort = (Long) j / del;
                    setMas.set(i, newElShort);
                    i++;
                    break;
            }
        }

    }

    /**
     * Метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его
     *
     * @param integer
     *
     */
    public void removeElement(Integer integer) {
        setMas.remove(integer);
    }

    @Override
    public String toString() {
        StringBuilder sbOut = new StringBuilder();
        sbOut.append("MathBox{ ");
        for (Object j : setMas) {
            sbOut.append(j).append(" ");
        }
        sbOut.append("}");

        return sbOut.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return Objects.equals(setMas, mathBox.setMas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(setMas);
    }
}

