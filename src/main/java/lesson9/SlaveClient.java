package lesson9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Клиентский поток для прослушки сервера
 */
public class SlaveClient extends Thread {
    Socket socket;
    boolean flag = true;

    public SlaveClient(Socket socket) {
        this.socket = socket;
        start();
    }


    @Override
    public void run() {
        super.run();

        while (flag) {
            try {
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(socket.getInputStream()));
                System.out.println(bufferedReader.readLine());
            } catch (IOException ex) {

                System.out.println("Ошибка в SlaveClient" + ex.getStackTrace());
                break;
            }

        }
    }
}
