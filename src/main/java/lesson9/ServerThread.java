package lesson9;

import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread {
    Socket socket;
    String message;
    InputStream fromClient;
    BufferedReader clientReader;
    String name;
    String nameInThePrivateMaessge;


    public ServerThread(Socket socket) throws IOException {
        this.socket = socket;
        fromClient = socket.getInputStream();
        clientReader = new BufferedReader(new InputStreamReader(fromClient));
        setPriority(MAX_PRIORITY);
        start();
    }

    /**
     * Логика работы сервера
     */
    @Override
    public void run() {
        super.run();
        try {
            sendToClient(socket, "Введите ваше имя:");
            name = clientReader.readLine();
            Server.addServerThread(name.toLowerCase(), socket);

            while (true) {
                {
                    message = clientReader.readLine();
                    if (message.equals("stop")) {
                        socket.close();
                        this.stop();
                    }

                    if (message.length() > 2 && message.substring(0, 3).equals("-pm")) {
                        message = parseString(message);
                        sendToClient(Server.mapServerThread.get(nameInThePrivateMaessge.toLowerCase()),name+ ": "+ message);
                        continue;
                    }


                    for (Socket socket : Server.arraySocket) {
                        if (socket.equals(this.socket))
                            continue;

                        sendToClient(socket, name + ": " + message);
                    }
                    System.out.println("Server read: " + message);
                }

            }
        } catch (IOException ex) {
            System.out.println("Ошибка в ServerThread.run " + ex.getStackTrace());
        }
    }

    /**
     * Отправка сообщения клиенту
     *
     * @param socket
     * @param msg
     * @throws Exception
     */
    private void sendToClient(Socket socket, String msg) {
        try {
            OutputStream toClient = socket.getOutputStream();
            BufferedWriter clientWriter = new BufferedWriter(new OutputStreamWriter(toClient));
            clientWriter.write(msg + "\n");
            clientWriter.flush();
        } catch (IOException ex) {
            System.out.println("Ошибка в ServerThread.sendToClient " + ex.getStackTrace());
        }

    }

    /** Метод убирает служебные опции для приватного сообщения
     * @param str - сообщение
     * @return
     */
    private String parseString(String str) {
        str = str.replaceAll("-pm ", "");
        nameInThePrivateMaessge = str.substring(0, str.indexOf(" "));
        str = str.replaceAll(nameInThePrivateMaessge, "");
        return str;
    }

}
