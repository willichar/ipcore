package lesson9;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Сервер
 * arraySocket - список всех сокетов,подключенных к серверу
 */
public class Server {

    public static final int PORT = 8881;
    private static ServerSocket serverSocket;
    public static List<Socket> arraySocket = new ArrayList<>();
    public static Map<String,Socket> mapServerThread = new HashMap<>();

    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(PORT);
            Server serv = new Server();
            while (true) {
                Socket socket = serverSocket.accept();
                arraySocket.add(socket);
                ServerThread st = new ServerThread(socket);


            }
        } catch (IOException ex) {
            System.out.println("Ошибка в Server" + ex.getStackTrace());
        } finally {
            try {
                if (!(serverSocket.isClosed())) {
                    serverSocket.close();
                }
            } catch (IOException ex) {
                System.out.println("Ошибка в Server при закрытии" + ex.getStackTrace());
            }

        }
    }

    public static void addServerThread(String name, Socket socket){
        mapServerThread.put(name,socket);
    }
}

