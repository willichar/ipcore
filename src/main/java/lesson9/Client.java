package lesson9;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", Server.PORT);

        SlaveClient sc = new SlaveClient(socket);
        BufferedWriter bufferedWriter =
                new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));


        Scanner scanner = new Scanner(System.in);
        String message;
        while (true) {
            message = scanner.nextLine();
            bufferedWriter.write(message);
            bufferedWriter.newLine();
            bufferedWriter.flush();

            if (message.equals("stop")) {
                sc.flag = false;
                break;
            }
        }

    }
}
