package lesson2.task3;

import lesson2.task3.Interfaces.Sort;

/**
 * Пузырьковая сортировка
 */
public class Bubblesort implements Sort {

    /**
     * Метод сортировки массива
     *
     * @param p - массив объектов Person
     * @return
     */
    @Override
    public Person[] sort(Person[] p) {
        boolean b = false;
        while (!b) {
            b = true;
            for (int i = 0; i < p.length; i++) {
                for (int j = i + 1; j < p.length; j++) {
                    if (p[i].getSex().compareTo(p[j].getSex()) > 0) {
                        Person pb = p[j];
                        p[j] = p[i];
                        p[i] = pb;
                        b = false;
                    }
                    if ((p[i].getAge() < p[j].getAge()) && p[i].getSex().equals(p[j].getSex())) {
                        Person pb = p[j];
                        p[j] = p[i];
                        p[i] = pb;
                        b = false;
                    }
                    if (p[i].getAge() == p[j].getAge() && (p[i].getSex().compareTo(p[j].getSex()) > 0)) {
                        if (p[i].getName().compareTo(p[j].getName()) > 0) {
                            Person pb = p[j];
                            p[j] = p[i];
                            p[i] = pb;
                            b = false;
                        }

                    }
                    break;
                }
            }
        }
        return p;
    }
}
