package lesson2.task3.Test;

import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;

import java.io.IOException;

public class Main {
    @Command // One,
    public String hello() {
        return "Hello, World!";
    }

    @Command // two,
    public int add(int a, int b) {
        return a + b;
    }
    public static void main(String[] args) throws IOException {
        ShellFactory.createConsoleShell("hello", "", new Main())
                .commandLoop();
    }
}
