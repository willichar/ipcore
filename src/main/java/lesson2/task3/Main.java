package lesson2.task3;

import lesson2.task3.Exception.NameAndAgeOtheExaption;
import lesson2.task3.Interfaces.Sort;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Main m = new Main();
        Sort bubbleort = new Bubblesort();
        Sort quicksort = new Quicksort();
        Person[] pBubbl = m.genData(1000);
        m.checkMas(pBubbl);
        Person[] pQuick = pBubbl.clone();
        long startTimeBubbl = System.nanoTime();
        bubbleort.sort(pBubbl);
        long stopTimeBubbl = System.nanoTime();
        long timeBubll = stopTimeBubbl - startTimeBubbl;
        long startTimeQuick = System.nanoTime();
        quicksort.sort(pQuick);
        long stopTimeQuick = System.nanoTime();
        long timeQuick = stopTimeQuick - startTimeQuick;
        m.out(pBubbl, bubbleort, timeBubll);
        m.out(pQuick, quicksort, timeQuick);
    }


    /**
     * Метод генерации значений
     *
     * @param n - кол-во объектов
     * @return - массив объектов Person
     */
    public Person[] genData(int n) {
        Person[] pers = new Person[n];
        String[] name = {"Корфел", "Красные", "Листья", "Шава", "Китрет", "Ролен", "Ротенель", "Фанн", "Наило", "Мелла",
                "Коэлана", "Улл", "Вулу", "Зеннина", "Уалошот", "Алусс", "Литоари", "Шлорх", "Зиалио", "Цефраук", "Уалошот",
                "Иеруша", "Хен'Тат", "Ахнир", "Зольэксил", "Илун", "Ксогоголь", "Урхний", "Лар'Ула", "Ахнир", "Зольэксил",
                "Димбл", "Кранпернап", "Элдон", "Гимбл", "Пивохлёб", "Ампен", "Пагген", "Донелл", "Двазамок", "Бимпноттин", "Колотушка",
                "Муррен", "Шамп", "Холг", "Овак", "Денч", "Сехмет", "Вассел", "Хвон", "Такара", "Кус", "Эделандросс", "Хун", "Зул",
                "Ирк", "Ирк", "Маннео", "'Бесстрашный'", "Като-Олави", "Куори", "Эланитино", "Неа", "Кола-Гилеана", "Иглат", "АнакалатайПетани"};

        Random ran = new Random();
        for (int i = 0; i < n; i++) {
            pers[i] = new Person(name[ran.nextInt(name.length)],
                    ran.nextInt(100),
                    ran.nextInt(2) == 1 ? Person.Sex.MAN : Person.Sex.WOMAN);
        }
        return pers;
    }

    /**
     * @param p    - массив для вывода
     * @param is   - тип сортировки
     * @param time - время сортировки
     */

    private void out(Person[] p, Sort is, long time) {
        if (is instanceof Bubblesort) {
            System.out.println("Пузырькова сортировка: ");
        } else {
            System.out.println("Быстрая сортировка: ");
        }

        for (int i = 0; i < p.length - 1; i++) {
            System.out.println(p[i]);
        }
        System.out.println("Время сортировки: " + time);
    }


    /**
     * Если имена людей и возраст совпадают, выбрасывать в программе пользовательское исключение.
     *
     * @param p - массив с данными
     */
    private void checkMas(Person[] p) {
        for (int i = 0; i < p.length - 1; i++) {
            for (int j = 0; j < p.length; j++) {
                if (p[i].getAge() == p[j].getAge() && p[i].getName().equals(p[j].getName()) && !(p[i].equals(p[j]))) {
                    try {
                        throw new NameAndAgeOtheExaption();
                    } catch (NameAndAgeOtheExaption nameAndAgeOtheExaption) {
                        System.out.println("Совпало имя и возраст у: " + p[i].hashCode() + " - " + p[i] + " " +
                                "и " + p[j].hashCode() + " - " + p[j]);
                    }
                }
            }
        }
    }
}
