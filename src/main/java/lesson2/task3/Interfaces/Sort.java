package lesson2.task3.Interfaces;

import lesson2.task3.Person;

/**
 * Интерфейс для сортировок
 */

public interface Sort {

    /**
     * Метод сортировки массива
     *
     * @param p - массив объектов Person
     * @return - отсортированный массив
     */
    public Person[] sort(Person[] p);
}
