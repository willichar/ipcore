package lesson2.task3;

import lesson2.task3.Interfaces.Sort;

/**
 * Быстрая сортировка
 */
public class Quicksort implements Sort {

    /**
     * Метод сортировки массива
     *
     * @param p - массив объектов Person
     * @return
     */
    @Override
    public Person[] sort(Person[] p) {

        int ind = 0;
        int indBegin = 0;
        int indEnd = 0;

        quicksortSex(p, 0, p.length - 1);

        for (int i = 0; i < p.length - 1; i++) {
            int j = i + 1;
            if (!(p[i].getSex().equals(p[j].getSex()))) {
                ind = i;
                break;
            }
        }
        quicksortAge(p, 0, ind);
        quicksortAge(p, ind + 1, p.length - 1);

        for (int i = 0; i < p.length - 1; i++) {
            if (p[i].getAge() == p[i + 1].getAge() && p[i].getSex().equals(p[i + 1].getSex())) {
                indBegin = i;
                while (p[i].getAge() == p[i + 1].getAge() && p[i].getSex().equals(p[i + 1].getSex())) {
                    i++;
                    if (i == p.length - 1) {
                        break;
                    }
                }
                indEnd = i;
                quicksortName(p, indBegin, indEnd);
            }

        }
        return p;
    }

    /**
     * Быстрая сортировка по полу
     * @param mas
     * @param first
     * @param last
     */
    private void quicksortSex(Person[] mas, int first, int last) {
        int mid;
        Person count;
        int f = first, l = last;
        mid = mas[Math.round((f + l) / 2)].getSex().charAt(0);
        do {
            while (mas[f].getSex().charAt(0) < mid) f++;
            while (mas[l].getSex().charAt(0) > mid) l--;
            if (f <= l) {
                count = mas[f];
                mas[f] = mas[l];
                mas[l] = count;
                f++;
                l--;
            }
        } while (f < l);
        if (first < l) quicksortSex(mas, first, l);
        if (f < last) quicksortSex(mas, f, last);
    }

    /**
     * Быстрая сортировка по возрасту
     * @param mas
     * @param first
     * @param last
     */
    private void quicksortAge(Person[] mas, int first, int last) {
        int mid;
        Person count;
        int f = first, l = last;
        mid = mas[Math.round((f + l) / 2)].getAge();
        do {
            while (mas[f].getAge() > mid) f++;
            while (mas[l].getAge() < mid) l--;
            if (f <= l) {
                count = mas[f];
                mas[f] = mas[l];
                mas[l] = count;
                f++;
                l--;
            }
        } while (f < l);
        if (first < l) quicksortAge(mas, first, l);
        if (f < last) quicksortAge(mas, f, last);

    }


    /**
     * Быстрая сортировка по имени
     * @param mas
     * @param first
     * @param last
     */
    private void quicksortName(Person[] mas, int first, int last) {
        int mid;
        Person count;
        int f = first, l = last;
        mid = mas[Math.round((f + l) / 2)].getName().charAt(0);
        do {
            while (mas[f].getName().charAt(0) < mid) f++;
            while (mas[l].getName().charAt(0) > mid) l--;
            if (f <= l) {
                count = mas[f];
                mas[f] = mas[l];
                mas[l] = count;
                f++;
                l--;
            }
        } while (f < l);
        if (first < l) quicksortName(mas, first, l);
        if (f < last) quicksortName(mas, f, last);
    }
}