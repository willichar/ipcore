package lesson2.task1;

import lesson2.task1.Exception.MyException;

public class Main {

    public static void main(String[] args)  {
        
        try {
            String npe = null;
            System.out.println("”Hello, World!”");
            System.out.println(npe.toUpperCase());
        } catch (NullPointerException nulp) {
            try {
                nulp.printStackTrace();
                String aioobe = "Hello, World!";
                System.out.println(aioobe.split(" ")[5]);
            } catch (IndexOutOfBoundsException ex){
                ex.printStackTrace();
            }
        } finally {
            try {
                throw new MyException();
            } catch (MyException e) {
                e.printStackTrace();
            }
        }

    }
}
