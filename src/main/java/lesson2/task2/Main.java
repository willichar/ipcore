package lesson2.task2;

import java.util.Random;

public class Main {

    /**
     * @param args кол-во элеентов в массиве
     */
    public static void main(String[] args) {
        try {

            int n = Integer.parseInt(args[0]);
            int[] mass = new int[n];
            Random rand = new Random();
            for (int i = 0; i < mass.length; i++) {
                mass[i] = rand.nextInt();
            }
            for (int i = 0; i < mass.length; i++) {
                double q = Math.sqrt(mass[i]);
                if (mass[i] < 0) {
                    try {
                        throw new Exception();
                    } catch (Exception ex) {
                        continue;
                    }
                }
                if (Math.pow((Math.round(q)), 2) == mass[i]) {
                    System.out.println(q);
                }
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Неверное кол-во элементов");
        } catch (NumberFormatException ex) {
            System.out.println("Неверный формат");
        }
    }
}