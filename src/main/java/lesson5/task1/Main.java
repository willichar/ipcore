package lesson5.task1;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        StringBuilder sb = new StringBuilder();
        try (InputStreamReader inputStream = new InputStreamReader(new FileInputStream("in.txt"), "UTF-8");
             FileOutputStream osw = new FileOutputStream("out.txt")) {
            int i = 0;
            while ((i = inputStream.read()) != -1) {
                sb.append((char) i);
            }
            List<String> listWords = new ArrayList<>(Arrays.asList(sb.toString().replaceAll("., ", " ").split(" "))); // List<String> listWords = Arrays.asList... приводит к UnsupportedOperationException при удалении элемента
            main.sort(listWords);
            String outString = listWords.toString();
            osw.write(outString.getBytes());
        } catch (IOException ex) {
            ex.getStackTrace();
        }
    }

    /**
     * Сортировка
     *
     * @param masS
     */
    public void sort(List<String> masS) {
        boolean b = false;
        while (!b) {
            b = true;
            for (int i = 0; i < masS.size(); i++) {
                for (int j = i + 1; j < masS.size(); j++) {
                    int compare = masS.get(i).toLowerCase().compareTo(masS.get(j).toLowerCase());
                    if (compare > 0) {
                        String temp = masS.get(j);
                        masS.set(j, masS.get(i));
                        masS.set(i, temp);
                        b = false;
                    } else if (compare == 0) {
                        masS.remove(j);
                    }
                }

            }
        }
    }
}
