package lesson5.task2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class Main {

    Random random = new Random();

    public static void main(String[] args) throws Exception {

        Main main = new Main();
        main.getFiles("d://",111,3);


    }

    public String[] genWords() {

        String[] words = new String[100];
        for (int i = 0; i < words.length; i++) {
            int numberChar = 1 + random.nextInt(14);
            char[] let = new char[numberChar];
            for (int j = 0; j < let.length; j++) {
                let[j] = (char) ('a' + random.nextInt(25));
            }
            words[i] = new String(let);
        }
        return words;
    }

    public String[] genSentence(int numberSent) {
        String[] sentenceMas = new String[numberSent];
        char[] znak = {'!', '.', '?'};
        for (int i = 0; i < sentenceMas.length; i++) {
            StringBuilder sb = new StringBuilder();
            String[] words = genWords();
            int numberWords = 1 + random.nextInt(14);
            for (int j = 0; j < numberWords; j++) {
                if (j == 0) {
                    int indexWord = random.nextInt(99);
                    String temp = words[indexWord].substring(0, 1).toUpperCase() + words[indexWord].substring(1, words[indexWord].length());
                    sb.append(temp);
                } else if (j == numberWords - 1) { //что бы не было пробела перед точкой
                    sb.append(words[random.nextInt(99)]);
                } else {
                    if (random.nextInt(99) % 3 == 0) {
                        sb.append(words[random.nextInt(99)] + ", ");
                    } else {
                        if (Math.random() < 0.9)

                        sb.append(words[random.nextInt(99)] + " ");
                    }

                }

            }
            sb.append(znak[random.nextInt(znak.length)] + " ");
            sentenceMas[i] = sb.toString();
        }
        return sentenceMas;
    }

    public StringBuilder genParagraph() {
        StringBuilder parag = new StringBuilder();
        int numSents = 1 + random.nextInt(19);
        String[] sent = genSentence(numSents);
        for (String s : sent) {
            parag.append(s);
        }
        parag.append("\r");
        System.out.println(parag.toString());
        return parag;
    }

    public void getFiles(String path, int size, int n) throws Exception {
        int numbetFile = 0;
        while (!(numbetFile == n)) {
            File file = new File(String.format("%stest_%s.txt",path,numbetFile+1));
            try (FileOutputStream fos = new FileOutputStream(file);
                 BufferedOutputStream bos = new BufferedOutputStream(fos)) {
                StringBuilder sb = new StringBuilder();
                while (sb.length() < size) {
                    sb.append(genParagraph());
                }
                bos.write(sb.toString().getBytes(), 0, size);


            }
            numbetFile++;
        }
    }
}


