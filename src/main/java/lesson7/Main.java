package lesson7;

import lesson2.task3.Person;
import lesson4.Pet;

import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Main m = new Main();
        User user = new User("Peter", 15, new Cat("Busya"), new Pet(1111, "Bars", new Person("Tolya", 55, "MAN"), 55));
        m.serialize(user, "user");

        User deSerUser = (User) m.deSerialize("user");
        System.out.println(deSerUser);
    }

    public void serialize(Object object, String file) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(object);
        } catch (FileNotFoundException ex) {
            ex.getMessage();
        } catch (IOException ex) {
            ex.getMessage();
        }
    }

    public Object deSerialize(String file) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return ois.readObject();
        } catch (FileNotFoundException ex) {
            ex.getMessage();
        } catch (IOException ex) {
            ex.getMessage();
        } catch (ClassNotFoundException ex) {
            ex.getMessage();
        }
        return null;
    }
}
