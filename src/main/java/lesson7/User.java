package lesson7;

import lesson4.Pet;

import java.io.Serializable;

public class User implements Serializable {
    private String name;
    private int age;
    private Cat cat;
    transient private Pet pet;

    public User(String name, int age, Cat cat, Pet pet) {
        this.name = name;
        this.age = age;
        this.cat = cat;
        this.pet = pet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", cat=" + cat +
                ", pet=" + pet +
                '}';
    }
}
