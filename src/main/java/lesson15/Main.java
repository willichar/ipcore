package lesson15;

import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Random;

public class Main {
    private static final Logger log = Logger.getLogger(Main.class);
    public static Random rand = new Random();
    static final String SQL = "INSERT INTO \"user\" (id, name, birthday, \"login_ID\", city, email, description) VALUES (?,?,?,?,?,?,?)";
    static String TEMPLATESQL = "INSERT INTO role (id, name) VALUES (%d,'%s')";

    public static void main(String[] args) {

        try (Connection con = ConnectDB.getConnection()) {
            prepareStatement(con);
            batchProcessing(con);
            selectDb("Tom", "Tom007", con);
            handManageDB(con);
            log.info("Все методы выполнены, подключение к БД закрыто");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * 2.a) Через jdbc интерфейс сделать запись данных(INSERT) в таблицу. Используя параметризированный запрос
     *
     * @param con
     */
    public static void prepareStatement(Connection con) {

        PreparedStatement st = null;
        try {

            st = con.prepareStatement(SQL);
            st.setInt(1, rand.nextInt(1000));
            st.setString(2, "Tom3");
            st.setString(3, "01.01.01");
            st.setString(4, "Tom007");
            st.setString(5, "Chicago");
            st.setString(6, "tom@gmail.com");
            st.setString(7, "descriprion");
            st.executeUpdate();
        } catch (SQLException ex) {
            log.error("Ошибка в prepareStatement: " + ex.getMessage());
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
        } finally {
            try {
                st.close();
                log.info("Метод prepareStatement выполнен.");

            } catch (SQLException e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
        }
    }

    /**
     * 2.b) Через jdbc интерфейс сделать запись данных(INSERT) в таблицу. Используя batch процесс
     *
     * @param con
     */
    public static void batchProcessing(Connection con) {
        Statement st = null;
        try {
            st = con.createStatement();
            con.setAutoCommit(false);
            st.addBatch(createQery(rand.nextInt(1000), Role.Administration));
            st.addBatch(createQery(rand.nextInt(1000), Role.Clients));
            st.addBatch(createQery(rand.nextInt(1000), Role.Billing));
            st.executeBatch();
            con.commit();
            log.info("Метод batchProcessing выполнен.");

        } catch (SQLException ex) {
            log.error(ex.getMessage());
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
        } finally {
            try {
                st.close();

            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    /**
     * 3)      Сделать параметризированную выборку по login_ID и name одновременно
     *
     * @param name
     * @param login_id
     * @param con
     */
    public static void selectDb(String name, String login_id, Connection con) {

        ResultSet rs = null;
        String sql = "select * from \"user\" u where u.name=? and  u.\"login_ID\"=?";
        try (PreparedStatement pst = con.prepareStatement(sql)) {

            pst.setString(1, name);
            pst.setString(2, login_id);
            rs = pst.executeQuery();
            int columns = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    System.out.println(rs.getString(i));
                }
            }
            log.info("Метод selectDb выполнен");

        } catch (SQLException e) {
            log.error(e.getMessage());
            try {
                con.close();
            } catch (SQLException e1) {
                log.error(e1.getMessage());
            }
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }

        }
    }

    /**
     * Выполнить 2-3 SQL операции на ваше усмотрение (например, Insert в 3 таблицы – USER, ROLE, USER_ROLE) между sql операциями установить точку сохранения (SAVEPOINT A),
     * намеренно ввести некорректные данные на последней операции, что бы транзакция откатилась к логической точке SAVEPOINT A
     *
     * @param con
     */
    public static void handManageDB(Connection con) {
        Statement st = null;
        Savepoint spoint = null;
        try {
            con.setAutoCommit(false);
            st = con.createStatement();
            st.executeUpdate(createQery(rand.nextInt(1000), Role.Administration));
            spoint = con.setSavepoint("spoint");
            st.executeUpdate(createQery(rand.nextInt(1000), Role.Billing));
            st.executeUpdate("insert into user_role (id, user_id, role_id) VALUES (2,1,999);"); //ошибочный запрос
            con.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
            try {
                con.rollback(spoint);
                log.info("Возрат к сейвпойнту:");
                con.commit();
            } catch (SQLException e1) {
                e1.printStackTrace();
                log.error("Возрат к сейвпойнту:" + e1.getMessage());
            }
        } finally {
            try {
                st.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static String createQery(int n, Role st) {
        String string = String.format(TEMPLATESQL, n, st);
        return string;
    }


}
