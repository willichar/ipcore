CREATE TABLE public.USER
(
    id character(255) NOT NULL,
    name character(255),
    birthday character(255),
    "login_ID" character(255),
    city character(255),
    email character(255),
    description character(255),
    CONSTRAINT "USER_pkey" PRIMARY KEY (id)
) TABLESPACE pg_default ;

CREATE TABLE public.ROLE(
id character(255) NOT NULL,
name character(255),
CONSTRAINT "ROLE_pkey" PRIMARY KEY (id)
) TABLESPACE pg_default;

CREATE TABLE public.USER_ROLE
(
    id character(255) NOT NULL,
    user_id character(255),
    role_id character(255),
    CONSTRAINT "USER_ROLE_pkey" PRIMARY KEY (id),
    CONSTRAINT "Role_id" FOREIGN KEY (role_id)
        REFERENCES public.ROLE (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "fgUser_id" FOREIGN KEY (user_id)
        REFERENCES public.USER (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE pg_default;

CREATE TABLE public."LOGS"
(
    id character(255),
    "DATE" date,
    "LOG_LEVEL" character(255),
    "MESSAGE" character(1000),
    "EXCEPTION" character(255)
)
TABLESPACE pg_default;

CREATE EXTENSION "uuid-ossp" SCHEMA public; --для генерации id