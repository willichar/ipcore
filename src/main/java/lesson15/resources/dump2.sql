--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: LOGS; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."LOGS" (
    id character(255),
    "DATE" character(255),
    "LOG_LEVEL" character(255),
    "MESSAGE" character(1024),
    "EXCEPTION" character(2000)
);


ALTER TABLE public."LOGS" OWNER TO postgres;

--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id character(255) NOT NULL,
    name character(255)
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id character(255) NOT NULL,
    name character(255),
    birthday character(255),
    "login_ID" character(255),
    city character(255),
    email character(255),
    description character(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    id character(255) NOT NULL,
    user_id character(255),
    role_id character(255)
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- Data for Name: LOGS; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."LOGS" (id, "DATE", "LOG_LEVEL", "MESSAGE", "EXCEPTION") FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, name) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, name, birthday, "login_ID", city, email, description) FROM stdin;
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role (id, user_id, role_id) FROM stdin;
\.


--
-- Name: role ROLE_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT "ROLE_pkey" PRIMARY KEY (id);


--
-- Name: user_role USER_ROLE_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT "USER_ROLE_pkey" PRIMARY KEY (id);


--
-- Name: user USER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "USER_pkey" PRIMARY KEY (id);


--
-- Name: user_role Role_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT "Role_id" FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: user_role fgUser_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT "fgUser_id" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

