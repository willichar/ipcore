package lesson15;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectDB {
    public static Connection getConnection() {
        try (FileInputStream fis = new FileInputStream("src\\main\\java\\lesson15\\resources\\Properties.properties")){

            Properties prop = new Properties();
            prop.load(fis);
            String url = prop.getProperty("pg.url");
            String login = prop.getProperty("pg.username");
            String password = prop.getProperty("pg.password");
            Connection con = DriverManager.getConnection(url, login, password);
            return con;
        } catch (Exception ex) {
            System.out.println("Ошибка в ConnectDB: " + ex.getStackTrace());
        }
        return null;
    }
}
