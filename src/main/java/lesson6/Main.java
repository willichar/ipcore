package lesson6;

import java.math.BigInteger;
import java.util.*;

public class Main {

    public Map <Integer,BigInteger> mapFactorial = new HashMap<>();
    static int[] d = genMas(10);
    static int numberThread = 0; //счетчик инициализированных потоков
    static int counterThreadRun=0; // счетчик выполненных потоков потоков

    public static void main(String[] args) throws Exception {

        System.out.println("Исходный массив:");
        Arrays.stream(d).forEach((int x) -> System.out.print(x + " "));
        Main main = new Main();

        while (numberThread != d.length) {
            Factorial m = new Factorial(d, main, numberThread);
            m.start();
            numberThread++;
        }
        main.outLst(main);
    }

    public static int[] genMas(int lenght) {
        int[] mass = new int[lenght];
        Random rand = new Random();
        for (int i = 0; i < lenght; i++) {
            mass[i] = rand.nextInt(100)+1;
        }
        return mass;
    }

    /**
     * Синхронизированный метод для вывода факториалов
     * @param m
     * @throws Exception
     */
    public synchronized void outLst(Main m) throws Exception {
        wait();
        System.out.println("\nФакториалы:");
        for (Integer key : mapFactorial.keySet()){
            System.out.println(key +" = " + mapFactorial.get(key) );
        }
    }

    /**
     * Синхронизированный метод для записи факториалов в список
     * @param m
     * @param bi
     * @param element
     */
    public synchronized void putListFac(Main m, BigInteger bi, int element) {
        counterThreadRun++;
        mapFactorial.put(element,bi);
        if (counterThreadRun == d.length) {
            notify();
        }
    }
}
