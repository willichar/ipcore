package lesson6;

import java.math.BigInteger;

public class Factorial extends Thread {
    private int[] mass;
    private Main m;
    private int i;


    /**
     * Конструктор
     *
     * @param mass - исходный массив
     * @param m    - главный объект
     * @param i    - счетчик потков
     */
    public Factorial(int[] mass, Main m, int i) {
        this.mass = mass;
        this.m = m;
        this.i = i;

    }

    @Override
    public void run() {
        super.run();
        factorial(i);

    }

    /**
     * Метод который рассчитывает факториал
     *
     * @param n
     */
    public void factorial(int n) {
        BigInteger fac = BigInteger.ONE;
        for (int i = 1; i <= mass[n]; i++) {
            fac = fac.multiply(BigInteger.valueOf(i));
        }
        m.putListFac(m, fac, mass[n]);

    }
}
